from flask import Flask, render_template


app = Flask(__name__,template_folder = 'templates')

@app.route("/")
def index():
    return ("UOCIS docker demo!")
@app.errorhandler(404)
def notfound(e):
    return render_template("404.html"), 404

@app.errorhandler(403)
def forbidden(e):
    return render_template("403.html"),403

if __name__ == "__main__":
    app.run(debug=True, host = '0.0.0.0')
    
